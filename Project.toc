\babel@toc {english}{}
\contentsline {chapter}{\hbox to\@tempdima {\hfil }Acknowledgements}{i}{Doc-Start}%
\contentsline {chapter}{\hbox to\@tempdima {\hfil }Preface}{ii}{Doc-Start}%
\contentsline {chapter}{\hbox to\@tempdima {\hfil }List of Figures}{v}{chapter*.1}%
\contentsline {chapter}{\numberline {1}Introduction to Cosmology}{1}{chapter.1}%
\contentsline {section}{\numberline {1.1}History of Cosmology}{1}{section.1.1}%
\contentsline {section}{\numberline {1.2}Modern Cosmology}{2}{section.1.2}%
\contentsline {subsection}{\numberline {1.2.1}Cosmological red-shift and Expanding Universe}{4}{subsection.1.2.1}%
\contentsline {subsection}{\numberline {1.2.2}General Theory of Relativity}{5}{subsection.1.2.2}%
\contentsline {subsection}{\numberline {1.2.3}Friedmann-Lema\^{i}tre-Robertson-Walker Metric}{6}{subsection.1.2.3}%
\contentsline {subsection}{\numberline {1.2.4}Friedmann Equations}{7}{subsection.1.2.4}%
\contentsline {subsubsection}{Fluid Equation}{8}{section*.3}%
\contentsline {subsubsection}{Hubble's law and red-shift in terms of scale factor}{9}{section*.4}%
\contentsline {subsection}{\numberline {1.2.5}Solving Friedmann equations}{9}{subsection.1.2.5}%
\contentsline {subsubsection}{Matter Dominated}{10}{section*.5}%
\contentsline {subsubsection}{Radiation Dominated}{10}{section*.6}%
\contentsline {subsection}{\numberline {1.2.6}de Sitter universe}{11}{subsection.1.2.6}%
\contentsline {section}{\numberline {1.3}Observational Parameters}{12}{section.1.3}%
\contentsline {subsection}{\numberline {1.3.1}Density Parameter}{12}{subsection.1.3.1}%
\contentsline {subsection}{\numberline {1.3.2}Decceleration Parameter}{13}{subsection.1.3.2}%
\contentsline {subsection}{\numberline {1.3.3}Hubble Parameter}{14}{subsection.1.3.3}%
\contentsline {chapter}{\numberline {2}Accelerated Expansion and $\Lambda $CDM Model}{15}{chapter.2}%
\contentsline {section}{\numberline {2.1}Type 1a Supernovae}{16}{section.2.1}%
\contentsline {section}{\numberline {2.2}Lambda-CDM($\Lambda $CDM) Model}{17}{section.2.2}%
\contentsline {subsection}{\numberline {2.2.1}Dark Matter}{18}{subsection.2.2.1}%
\contentsline {subsubsection}{Evidences of Dark Matter}{19}{section*.7}%
\contentsline {paragraph}{Galaxy rotation Curve:}{19}{section*.9}%
\contentsline {paragraph}{X-Ray Studies of Intergalactic Gas:}{19}{section*.10}%
\contentsline {paragraph}{Gravitational Lensing:}{20}{section*.11}%
\contentsline {subsubsection}{Cosmological Constant as Dark Energy}{20}{section*.12}%
\contentsline {section}{\numberline {2.3}Evolution of Parameters}{21}{section.2.3}%
\contentsline {subsection}{\numberline {2.3.1}Scale Factor, a(t)}{21}{subsection.2.3.1}%
\contentsline {subsection}{\numberline {2.3.2}Hubble Parameter}{23}{subsection.2.3.2}%
\contentsline {subsection}{\numberline {2.3.3}Deceleration Parameter}{25}{subsection.2.3.3}%
\contentsline {section}{\numberline {2.4}Evaluation of Luminosity Distance vs red-shift}{27}{section.2.4}%
\contentsline {chapter}{\hbox to\@tempdima {\hfil }Bibliography}{31}{chapter*.13}%
\contentsline {chapter}{\hbox to\@tempdima {\hfil }Appendix}{33}{chapter*.14}%
\contentsline {chapter}{\numberline {A}\large {The data obtained by Stern et al. from passive evolution of galaxies}}{33}{appendix.A}%
\contentsline {chapter}{\numberline {B}\large {The data obtained from 307 galaxies}}{34}{appendix.B}%
