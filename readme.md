### This project contains the LaTeX files of my Masters Thesis on the topic **Standard Lambda-CDM Model: A Review**

#### The work was done under the guidance of **Prof. Titus K Mathew**, *Professor, Department of Physics, Cocin University of Science and Technology, Kochi.* 

#### I express my sincere thanks to [Supin P Surendran](https://www.researchgate.net/profile/Supin_Surendran) for the LateX template and initial lessons of LaTeX.

